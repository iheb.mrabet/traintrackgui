#ifndef TESTGUI_H
#define TESTGUI_H

#include <QStackedWidget>
#include <QWidget>
#include <QComboBox>
#include <QString>
#include <QMap>

#include "Connector.h"
#include "TestSpeed.h"
#include "TestLed.h"
#include "TestWindMill.h"
#include "TestWeiche.h"


class TestGui : public QWidget
{
    Q_OBJECT
    public:
        TestGui();

        void addWidget(QWidget* widget, const QString &name);

    public slots:
        void setWidgetAccordingToSelection(const QString&);

    private:

	QStackedWidget *selectionWidget;
        QWidget *placeHolder;
        QComboBox *combo_box_widget_selecetion;
        QMap<QString,QWidget*> widgets_collection;
};


#endif //TESTGUI_H
