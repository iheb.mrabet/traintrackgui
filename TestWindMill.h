#ifndef TESTWINDMILL_H
#define TESTWINDMILL_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QTextStream>

#include "Connector.h"

#define WINDMILL_THRESHOLD 20

class TestWindMill : public QWidget
{
	Q_OBJECT

	public:

		TestWindMill(QWidget *parent);

	public slots:

		void SpeedChanged();
		void DisplayChanged(int);

	private:
		
		QSlider * speed_sld;
		QLabel * speed_display;		

		int current_speed = 0; // 0 %

};


#endif //TESTWINDMILL_H
