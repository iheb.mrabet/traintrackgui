#ifndef CONNECTOR_H
#define CONNECTOR_H

#include "src_winterface/winterface-api.h"
#include <vector>

typedef unsigned int TrainID;

#define NUMBER_OF_TRAINS 2
#define NUMBER_OF_WEICHE 2

#define TRAIN_1 0
#define TRAIN_2 1

#define WINDMILL 11

#define TRACK1 0
#define TRACK2 1

#define API Connector::N() 

class Connector
{
    public:

        static Connector* N()
        {
            if(!_API)
            {
                _API = new Connector();
            }
            return _API;
        }


        bool connect();
        bool connectDummy();


        bool setLed(int led, bool set);
        bool flushLed();

        bool setSpeed(TrainID id, int speed); // id = [0,1] -> [Zug1,Zug2]  speed = [0,100]
        bool setDirection(TrainID id, int direction); //direction = [0,1] = [vorwaerts, rueckwaerts]
        bool setWeiche(int Weiche, bool direction);

        bool setTrack(int id); // id = [0,1] -> [kleiner Track, großer Track]

        bool setWindradSpeed(int speed);

    private:

        static Connector* _API;

        Connector(){}
        bool connection_successful = false;
        

        WINTP Zug[NUMBER_OF_TRAINS];

        WINTP mainboard;

};

#endif

