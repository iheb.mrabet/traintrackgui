#ifndef TESTLED_H
#define TESTLED_H

#include <QWidget>
#include <QObject>
#include <QGridLayout>
#include <QPushButton>
#include <QMap>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include "Connector.h"


class TestLed : public QWidget
{
	Q_OBJECT
	public:
		
		TestLed(QWidget* parent);
	
	public slots:
		void btn_pressed();
		void everything_on();
		void everything_off();

	private:

		QMap<QPushButton*,int> button_to_led; 	
		QMap<QPushButton*,bool> button_toggle;
};


#endif //TESTLED_H
