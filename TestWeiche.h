#ifndef TESTWEICHE_H
#define TESTWEICHE_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include "Connector.h"

class TestWeiche : public QWidget
{
	Q_OBJECT
	public:
		TestWeiche(QWidget*parent);

	public slots:

		void track1choosen();
		void track2choosen();

	private:
		
		bool track1_t_track2_f = true; 

};

#endif //TESTWEICHE_H
