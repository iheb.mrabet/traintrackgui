#ifndef CHANGETRACK_H
#define CHANGETRACK_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>

#include "ChangeTrackController.h"
#include "Definitions.h"

class ChangeTrack : public QLabel
{
	Q_OBJECT
	
	public:
		ChangeTrack(QWidget * parent);
	 
		void mousePressEvent(QMouseEvent *event) override;
	
	signals:

		void TrackClicked();
		
	public slots:

		void TrackChoosen(int);

	private:
		
		void setupMedia();
		void setupConnection();	
		void setupStyle();

		ChangeTrackController *changetrackcontroller;

		QPixmap *qpixmap_inner_track;
		QPixmap *qpixmap_outer_track;
};


#endif //CHANGETRACK_H
