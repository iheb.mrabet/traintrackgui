#include "ChangeTrackController.h"

ChangeTrackController::ChangeTrackController(QObject *view)
{
	connect(view, SIGNAL(TrackClicked()),
		this, SLOT(TrackClicked()));

	connect(this, SIGNAL(TrackChoosen(int)),
		view, SLOT(TrackChoosen(int)));
	
	setTrack(TRACK::OUTER);
}


void ChangeTrackController::TrackClicked()
{
	switch(currentTrack)
	{
		case TRACK::OUTER:
			setTrack(TRACK::INNER);
			break;
		case TRACK::INNER:
			setTrack(TRACK::OUTER);
			break;
	}
}

void ChangeTrackController::setTrack(TRACK track)
{
	currentTrack = track;

	API->setTrack(track);
	emit TrackChoosen(currentTrack);
}
