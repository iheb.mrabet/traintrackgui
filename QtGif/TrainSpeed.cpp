#include "TrainSpeed.h"
#include <QTransform>

TrainSpeed::TrainSpeed(QWidget * parent, TrainID id)
: QWidget(parent), trainID(id)
{
    trainspeedcontroller_data = new TrainSpeedController(this, id);
    setupGUI();
    setupConnection();
    updateDirection(DIRECTION::forward);
    updateSpeedDisplay(0);
}






void TrainSpeed::updateDirection(int direction)   
{
	switch(direction)
	{
		case DIRECTION::forward:
		      	for(int i = 0; i<3; i++)
			{
			     qlabel_forward_indicator[i]->show();
            		     qlabel_reverse_indicator[i]->hide();
			}
			break;	
		case DIRECTION::backward:
		      	for(int i = 0; i<3; i++)
			{
				qlabel_reverse_indicator[i]->show();
            			qlabel_forward_indicator[i]->hide();

			}
			break;	
		
	}

}



void TrainSpeed::updateSpeedDisplay(int speed)
{
    QString tmp;
    QTextStream(&tmp) << "Geschwindigkeit: "<<speed<<"%";
    qlabel_speed_display->setText(tmp); 
}

void TrainSpeed::updateStartStopButton(bool btn_state)
{
    if(btn_state)
    {
        //set button to start 
	qbutton_start_and_stop->setText(tr("Start"));
    }
    else
    {
        //set button to stop
	qbutton_start_and_stop->setText(tr("Stopp"));
    }
}

//Helper slot function
void TrainSpeed::updateSpeed()
{
    int speed = qslider_speed_control->value();

    emit change_speed(speed);
}

void TrainSpeed::setupConnection()
{
    connect(qbutton_change_direction, SIGNAL(pressed()),
            trainspeedcontroller_data, SLOT(change_direction()));

    connect(qbutton_start_and_stop, SIGNAL(pressed()),
            trainspeedcontroller_data, SLOT(start_stop()));

	
    connect(qslider_speed_control, SIGNAL(valueChanged(int)),
            this, SLOT(updateSpeedDisplay(int)));


    // We need to this because QSlider's sliderReleased SIGNAL doesn't 
    // transmit the slider's value 
    connect(qslider_speed_control, SIGNAL(sliderReleased()),
            this, SLOT(updateSpeed()));

    //A helper signal to transmit the slider's value to the
    //controller.
    connect(this, SIGNAL(change_speed(int)),
            trainspeedcontroller_data, SLOT(change_speed(int)));

    connect(this, SIGNAL(stop_all()),
             trainspeedcontroller_data, SLOT(stop()));

}

void TrainSpeed::setupGUI()
{
    QString buttonStyleNormal = "QPushButton { background-color: #0F172A; color: white; border: none; border-radius: 8px; padding: 5px; width: 500px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";
    QString buttonStyleRound = "QPushButton { background-color: white; color: black; border: 1px; border-radius: 25px; padding: 5px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";
    QString labelStyle = "QLabel { color: #0F172A; font-family : Inter; font-size : 25px; font-style : normal; font-weight : 800;}";
    QString sliderStyle = R"(
QSlider::groove:horizontal {
    border: 1px solid #999999;
    height: 8px;
    background: #e1e1e1;
    margin: 2px 0;
    border-radius: 4px;
}

QSlider::handle:horizontal {
    background: #333333;
    border: 1px solid #5c5c5c;
    width: 18px;
    height: 18px;
    margin: -4px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
    border-radius: 8px;
}

QSlider::handle:horizontal:hover {
    background: #555555;
    border-color: #666666;
}

QSlider::sub-page:horizontal {
    background: #666666;
    border: 1px solid #999999;
    height: 10px;
    border-radius: 4px;
}
)";



    loadImage(); 

    for(int i = 0; i < 3; i++)
    {
        qlabel_reverse_indicator[i] = new QLabel(this); 
        qlabel_reverse_indicator[i]->setPixmap(*qpixmap_left_arrow);

        QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect(this);
        qlabel_reverse_indicator[i]->setGraphicsEffect(effect);

        QPropertyAnimation *animation = new QPropertyAnimation(effect, "opacity", this);
        animation->setDuration(1000);
        animation->setStartValue(1.0); 
        animation->setEndValue(0.0);
        animation->setEasingCurve(QEasingCurve::InOutQuad);
        animation->setLoopCount(-1);
        animation->start();
    }

        for(int i = 0; i < 3; i++)
    {
        qlabel_forward_indicator[i] = new QLabel(this); 
        qlabel_forward_indicator[i]->setPixmap(*qpixmap_right_arrow);

        QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect(this);
        qlabel_forward_indicator[i]->setGraphicsEffect(effect);

        QPropertyAnimation *animation = new QPropertyAnimation(effect, "opacity", this);
        animation->setDuration(1000);
        animation->setStartValue(1.0);
        animation->setEndValue(0.0);
        animation->setEasingCurve(QEasingCurve::InOutQuad);
        animation->setLoopCount(-1);
        animation->start();
    }


    qlabel_train_image = new QLabel();
    qlabel_train_image->setPixmap(*qpixmap_train);


    qlabel_speed_display = new QLabel();
    qlabel_speed_display->setText(tr("Geschwindigkeit: 0 %"));
    qlabel_speed_display->setStyleSheet(labelStyle);
        
    QIcon qicon_change_direction(*qpixmap_direction_icon);
    qbutton_change_direction = new QPushButton("");
    QIcon directionIcon(PATH_CHANGE_DIRECTION_ICON);
    qbutton_change_direction->setIcon(directionIcon);
    qbutton_change_direction->setIconSize(QSize(40, 40));
    qbutton_change_direction->setStyleSheet(buttonStyleRound);

    

    qbutton_start_and_stop = new QPushButton("Start"); 
    QIcon playTrainIcon(PATH_PLAY_BUTTON_ICON);
    qbutton_start_and_stop->setIcon(playTrainIcon);
    qbutton_start_and_stop->setIconSize(QSize(40, 40));
    qbutton_start_and_stop->setStyleSheet(buttonStyleNormal);

    qslider_speed_control = new QSlider(Qt::Horizontal);
    qslider_speed_control->setRange(0,100);
    qslider_speed_control->setStyleSheet(sliderStyle);

    //connectiondisplay_connected = new ConnectionDisplay(this, trainspeedcontroller_data->getTrainID() );

    //--------------------------Layout-Setup---------------------------------------------

    QVBoxLayout *qvbox_main_layout = new QVBoxLayout(this);

    QHBoxLayout *qhbox_direction = new QHBoxLayout();

    QVBoxLayout *qvbox_backward_labels = new QVBoxLayout();
    QVBoxLayout *qvbox_forward_labels = new QVBoxLayout();

    QVBoxLayout *qvbox_connection = new QVBoxLayout();

    QHBoxLayout *qhbox_buttons = new QHBoxLayout();
    
    //---------------------------Layout-Fill---------------------------------------------
    
    for(int i = 0; i < 3; i++)
    {
        qvbox_backward_labels->addWidget(qlabel_reverse_indicator[i]); 
        qvbox_forward_labels->addWidget(qlabel_forward_indicator[i]); 
    }

//    qvbox_connection->addWidget(connectiondisplay_connected);
//   qvbox_connection->addStretch(3);


    qhbox_direction->addStretch(); 
    qhbox_direction->addLayout(qvbox_backward_labels);
    qhbox_direction->addWidget(qlabel_train_image);   
    qhbox_direction->addLayout(qvbox_forward_labels);
    qhbox_direction->addLayout(qvbox_connection);
    qhbox_direction->addStretch();

    qhbox_buttons->addStretch(0);
    qhbox_buttons->addWidget(qbutton_start_and_stop);
    qhbox_buttons->addWidget(qbutton_change_direction);
    qhbox_buttons->addStretch(0);

    qvbox_main_layout->addLayout(qhbox_direction);
    qvbox_main_layout->addWidget(qslider_speed_control);
    qvbox_main_layout->addWidget(qlabel_speed_display);
    qvbox_main_layout->addLayout(qhbox_buttons);              

    setLayout(qvbox_main_layout);


}

void TrainSpeed::loadImage()
{


	
    qpixmap_left_arrow = new QPixmap();
    qpixmap_left_arrow->load(PATH_DIRECTION_ARROW_LEFT);
    *qpixmap_left_arrow = qpixmap_left_arrow->scaled(60,60);	
    

    qpixmap_right_arrow = new QPixmap();
    qpixmap_right_arrow->load(PATH_DIRECTION_ARROW_RIGHT);
    *qpixmap_right_arrow = qpixmap_right_arrow->scaled(60,60);	

    qpixmap_placeholder_arrow = new QPixmap();
    qpixmap_placeholder_arrow->load(PATH_DIRECTION_PLACEHOLDER); 
    *qpixmap_placeholder_arrow = qpixmap_placeholder_arrow->scaled(60,60);


    qpixmap_train = new QPixmap();

    switch(trainID)
    {
	case TrainID::YELLOW:
    		qpixmap_train->load(PATH_YELLOW_TRAIN_ICON); 
		break;
	case TrainID::BROWN:
		qpixmap_train->load(PATH_BROWN_TRAIN_ICON);
    }

    qpixmap_direction_icon = new QPixmap();
    qpixmap_direction_icon->load(PATH_CHANGE_DIRECTION_ICON);

    qpixmap_play_icon = new QPixmap();
    qpixmap_play_icon->load(PATH_PLAY_BUTTON_ICON);

    
}

void TrainSpeed::stop()
{
	emit stop_all();
}

void TrainSpeed::setupStyle()
{

}
