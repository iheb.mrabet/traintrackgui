#ifndef LEDSELECTION_H
#define LEDSELECTION_H

#include <QWidget>
#include <QLabel>
#include <QPainter>
#include <QPushButton>
#include <utility>
#include <QMouseEvent>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "LedSelectionController.h"
#include "Definitions.h"

#define BIG_HOUSE_TOP 1
#define BIG_HOUSE_DOWN 2
#define BIG_HOUSE_SIDE 3
#define SMALL_HOUSE 4



struct LED
{
	LED(int id ,double x, double y, double scale)
	: LEDid(id), x(x), y(y), scale_of_LED(scale)
	{
		on = false;
	}

	bool intersect(const QPoint &point)
	{
		QRect bound(x*refWidth,
	               	y*refHeight,
		      	width/refWidthOriginal * refWidth,
		       	height/refHeightOriginal *  refHeight);
		return bound.contains(point);
	}

	void setOriginal(double _width, double _height)
	{
		refWidthOriginal = _width;
		refHeightOriginal = _height;
	}

	void setReferenceImage(double _width, double _height)
	{
		refWidth = _width;
		refHeight = _height;
	}

	void setOn(bool status)
	{
		this->on = status;
		
		on ? setImage(qpixmap_on) : setImage(qpixmap_off);

	}
	
	bool getOn()
	{
		return on;
	}

	void toggle()
	{
		setOn(!on);
	}

	void scaleLED(double scale)
	{
		scale_of_LED = scale;
	}
	
	void setupTextures(QPixmap *on, QPixmap *off)
	{
		qpixmap_on = on;
		qpixmap_off = off;

		on ? setImage(qpixmap_on) : setImage(qpixmap_off);
		
	}

	

	QPixmap getScaledImage()
	{
			return qpixmap_texture->scaled(width/refWidthOriginal *refWidth,
					       height /refHeightOriginal *refHeight);
	}

	QPoint getPos()
	{
		return QPoint(x * refWidth, y * refHeight);
	}
	
	void setImage(QPixmap * pixmap)
	{
		qpixmap_texture = pixmap;
		width =  qpixmap_texture->width();
		height = qpixmap_texture->height();
	}

		int LEDid;	
	private:
		

		double x,y; // x,y = [0,1]
		double width,height;
		double refWidthOriginal, refHeightOriginal;
		double refWidth; // width of the image house
		double refHeight; // height of the image house
		double scale_of_LED; 
		QPixmap * qpixmap_texture;
		QPixmap * qpixmap_on;
		QPixmap * qpixmap_off;
		bool on;	
};

class Selection : public QLabel
{
	Q_OBJECT
	public:
		Selection(QWidget*parent, 
			   const QString &path_house,
	              	   const QString& path_led_on,
			   const QString& path_led_off,
			   QList<LED> &&list);


		void resizeEvent(QResizeEvent *ev)override;
		void mousePressEvent(QMouseEvent *ev) override;

	signals:
	
		void setLed(int,bool);	

	public slots:
		
		void everything_on();
		void everything_off();

	private:
	
		QList<LED> qlist_leds;
		QPixmap *qpixmap_house;
		QPixmap *qpixmap_led_on;
		QPixmap *qpixmap_led_off;
		
		QPixmap *qpixmap_result;

		SelectionController *selectioncontroller;
		//May consider unique_ptr here, cause i'm too lazy

		void drawResultImage();
	
		void LED_CODE(int i, bool on);

};


class LedSelection : public QWidget
{
	Q_OBJECT
	
	public:

	LedSelection(QWidget * parent);


	signals:


	public slots:


	private:
	
		LedSelectionController * ledselectioncontroller;

		Selection * selection_big_house;
		Selection * selection_small_house;

		QPushButton *qpushbutton_all_on;
		QPushButton *qpushbutton_all_off;



};	



#endif //LEDSELECTION_H
