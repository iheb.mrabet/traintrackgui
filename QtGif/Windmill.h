#ifndef WINDMILL_H
#define WINDMILL_H

#include <QWidget>
#include <QMovie>
#include <QLabel>
#include <QSlider>
#include <QVBoxLayout>
#include <QTextStream>

#include "Definitions.h"
#include "WindmillController.h"

class WindmillController;

class Windmill : public QWidget
{
	Q_OBJECT
	
	public:
	
		Windmill(QWidget *parent = nullptr);
		void resizeEvent(QResizeEvent *ev)override;

	signals:

		void speedChanged(int);

	public slots:	
		
		void updateSpeedDisplay(int);
		
		
		void speedChangedHelper();

	private:


		QMovie *qmovie_windmill_gif;
		QLabel *qlabel_windmill;
		QLabel *qlabel_speed_display;
		QSlider *qslider_speed_control;
	
		WindmillController *windmillcontroller_control;

		void setupGUI();
		void setupStyle();
		void setupMedia();
		void setupConnection();
};		




#endif //WINDMILL_H
