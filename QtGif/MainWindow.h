#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QIcon>

#include "MainWindowController.h"
#include "LedSelection.h"
#include "TrainSpeed.h"
#include "ChangeTrack.h"
#include "Windmill.h"


class MainWindow : public QWidget
{
	Q_OBJECT

	public:
		MainWindow(QWidget * parent = nullptr);

		void closeEvent(QCloseEvent * ev) override;

	signals:

		void resetAll();
		void reconnect();

	public slots:
		void updateReconnect();

		void setReconnectButton();

	private:
	
		void setupGUI();
		void setupSizes();
		
		LedSelection *ledselection_panel;
		TrainSpeed *trainspeed_yellow_panel;
		TrainSpeed *trainspeed_brown_panel;
		ChangeTrack *changetrack_panel;
		Windmill *windmill_panel;

		QPushButton *qpushbutton_stop_all_trains;
		QPushButton *qpushbutton_reconnect;

		MainWindowController *mainwindow_controller;

		QLabel * qlabel_cool_train;

		

	
};

#endif //MAINWINDOOW_H
