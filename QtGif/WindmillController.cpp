#include "WindmillController.h"



WindmillController::WindmillController(QObject * view)
{
	connect(view, SIGNAL(speedChanged(int)),
		this, SLOT(speedChanged(int)));	
}


void WindmillController::speedChanged(int speed)
{
	if(speed < WINDMILL_THRESHOLD && current_speed < 3)
	{
		//Windmill won't start at low rpm
		//Let it first reach its threshold rpm
		//and then go down.

		API->setWindradSpeed(WINDMILL_THRESHOLD);
		sleep(0.5);
	}

	API->setWindradSpeed(speed);
	current_speed = speed;
}
