#include "MainWindowController.h"



MainWindowController::MainWindowController(QObject *view)
{

	connect(this, SIGNAL(updateReconnect()),
		view, SLOT(updateReconnect()));

	API->connect();
}


void MainWindowController::stop_all()
{
	API->setSpeed(TrainID::YELLOW, 0);
	API->setSpeed(TrainID::BROWN, 0);
}

void MainWindowController::reconnect()
{
	Reconnect *worker = new Reconnect();
	connect(worker, SIGNAL(resultReady()), this, SLOT(resetAll()));
	connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));



	worker->start();


}

void MainWindowController::resetAll()
{
	API->setSpeed(TrainID::YELLOW, 0);
	API->setSpeed(TrainID::BROWN, 0);
	
	API->setWindradSpeed(0);

	API->resetLED();

	emit updateReconnect();
}


void Reconnect::run()
{
	API->connect();
	emit resultReady();
}
