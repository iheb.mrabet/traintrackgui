#ifndef CONNECTIONDISPLAYCONTROLLER_H
#define CONNECTIONDISPLAYCONTROLLER_H

#include <QObject>
#include <QWidget>
#include "Connector.h"

class ConnectionDisplayController : public QObject
{
    Q_OBJECT

    public:

        ConnectionDisplayController(QObject * view, TrainID id);

    signals:

    

    public slots:



    private:

    TrainID id;

};


#endif //CONNECTIONDISPLAYCONTROLLER_H
