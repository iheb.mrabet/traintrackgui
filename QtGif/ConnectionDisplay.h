#ifndef CONNECTIONDISPLAY_H
#define CONNECTIONDISPLAY_H

#include <QLabel>
#include <QWidget>
#include <QPixmap>
#include <QIcon>
#include <QTimer>

#include "ConnectionDisplayController.h"
#include "Definitions.h"


class ConnectionDisplayController;

class ConnectionDisplay : public QLabel
{
    Q_OBJECT

    public:
        ConnectionDisplay(QWidget * parent, TrainID id);

    signals:


    public slots:

        void updateConnectionDisplay(bool); 

    private:    

        QTimer *qtimer_5sec;

        QPixmap *qpixmap_connection_online;
        QPixmap *qpixmap_connection_offline;

        ConnectionDisplayController *connectiondisplay_controller;
};



#endif //CONNECTIONDISPLAY_H
