#ifndef TRAINSPEEDCONTROLLER_H
#define TRAINSPEEDCONTROLLER_H

#include <QObject>
#include "TrainSpeed.h"

#include "Connector.h"

#define SLIDER_SPEED_MINIMUM 0
#define SLIDER_SPEED_MAXIMUM 100

#define MAXIMUM_SPEED 3900

class TrainSpeedController : public QObject
{
    Q_OBJECT

    public:

        TrainSpeedController(QObject * view, TrainID id);

	TrainID getTrainID(){return train_id;};
    signals:

        void updateDirection(int);
        void updateSpeedDisplay(int);
        void updateStartStopButton(bool);

    public slots:

        void change_speed(int);
        void change_direction();
        void start_stop();
	void stop();

    private:



        //Data
        DIRECTION current_direction;
        int current_speed;
        TrainID train_id;
        bool train_running;
        
};


#endif  //TRAINSPEEDCONTROLLER_H

