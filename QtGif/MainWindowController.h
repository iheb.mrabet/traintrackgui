#ifndef MAINWINDOWCONTROLLER_H
#define MAINWINDOWCONTROLLER_H

#include <QObject>
#include <QThread>

#include "Connector.h"



class Reconnect : public QThread
{
	Q_OBJECT
	
	void run() override;

signals:

	void resultReady();
	
		
};



class MainWindowController : public QObject
{
	Q_OBJECT
	public:
		MainWindowController(QObject * view);

	signals:
		void updateReconnect();


	public slots:
		void stop_all();
		void reconnect();

		void resetAll();

	private:
	


};

#endif //MAINWINDOWCONTROLLER_H
