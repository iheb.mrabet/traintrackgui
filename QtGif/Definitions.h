#define PATH_YELLOW_TRAIN_ICON "./res/gelb.png"
#define PATH_BROWN_TRAIN_ICON "./res/braune.png"
#define PATH_DIRECTION_ARROW_LEFT "./res/arrow_left.png"
#define PATH_DIRECTION_ARROW_RIGHT "./res/arrow_right.png"
#define PATH_DIRECTION_PLACEHOLDER "./res/placeholder.png"

#define PATH_CHANGE_DIRECTION_ICON "./res/icon/arrow-left-right.png"
#define PATH_PLAY_BUTTON_ICON "./res/icon/play.png"
#define PATH_CONNECTION_ONLINE "./res/icon/wifi.png"
#define PATH_CONNECTION_OFFLINE "./res/icon/wifi-off.png"

#define PATH_WINDMILL_GIF "./res/windmill.gif"

#define PATH_INNER_TRACK "./res/trackmid.png"
#define PATH_OUTER_TRACK "./res/trackout.png"


#define PATH_BIG_HOUSE "./res/bulb.png"
#define PATH_SMALL_HOUSE "./res/bulbs.png"
#define PATH_LIGHT_ON "./res/on.png"
#define PATH_LIGHT_OFF "./res/off.png"
