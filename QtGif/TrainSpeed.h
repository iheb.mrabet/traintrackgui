#ifndef TRAINSPEED_H
#define TRAINSPEED_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QPixmap>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QString>
#include <QTextStream>
#include <QIcon>
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>

#include "TrainSpeedController.h"
#include "ConnectionDisplay.h"
#include "Definitions.h"

class TrainSpeedController;
 


class TrainSpeed : public QWidget
{
    Q_OBJECT

    public:

        TrainSpeed(QWidget * parent, TrainID id);


    signals: 

        void change_speed(int);
	void stop_all();

    public slots:

        void updateDirection(int);
        void updateSpeedDisplay(int);
        void updateStartStopButton(bool);
	void stop();

        void updateSpeed();

    private:
        QLabel *qlabel_reverse_indicator [3]; 
        QLabel *qlabel_forward_indicator [3];
        QLabel *qlabel_train_image;
        QLabel *qlabel_speed_display;
        

        QPushButton *qbutton_start_and_stop;
        QPushButton *qbutton_change_direction;

        QSlider *qslider_speed_control;

        //ConnectionDisplay *connectiondisplay_connected;

        TrainSpeedController *trainspeedcontroller_data;

        QPixmap *qpixmap_left_arrow;
        QPixmap *qpixmap_right_arrow;
        QPixmap *qpixmap_train;
        QPixmap *qpixmap_direction_icon;
        QPixmap *qpixmap_play_icon;
        QPixmap *qpixmap_placeholder_arrow; 


        void setupGUI();
        void setupConnection();
        void loadImage();
	void setupStyle();

	int trainID;	
};


#endif //TRAINSPEED_H
