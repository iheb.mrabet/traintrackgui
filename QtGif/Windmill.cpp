#include "Windmill.h"



Windmill::Windmill(QWidget *parent)
: QWidget(parent)
{
	windmillcontroller_control = new WindmillController(this);

	setupMedia();
	setupGUI();
	setupStyle();
	setupConnection();

	resizeEvent(nullptr);

}

void Windmill::updateSpeedDisplay(int a)
{
	// Set Label Display Text
	QString tmp;	
	QTextStream(&tmp) << "Geschwindigkeit: "<< a <<"%";
	qlabel_speed_display->setText(tmp);

	// Set Movie playback speed
	if(a == 0)
	{
		qmovie_windmill_gif->stop();
		qmovie_windmill_gif->start();
		qmovie_windmill_gif->stop();
	}
	else
	{
		qmovie_windmill_gif->start();
		qmovie_windmill_gif->setSpeed(a+70);	
	}
}
	
void Windmill::speedChangedHelper()
{
	int a = qslider_speed_control->value();
	emit speedChanged(a);
}

void Windmill::setupGUI()
{

    QString labelStyle = "QLabel { color: #0F172A; font-family : Inter; font-size : 20px; font-style : normal; font-weight : 800;}";

    QString sliderStyle = R"(
QSlider::groove:horizontal {
    border: 1px solid #999999;
    height: 8px;
    background: #e1e1e1;
    margin: 2px 0;
    border-radius: 4px;
}

QSlider::handle:horizontal {
    background: #333333;
    border: 1px solid #5c5c5c;
    width: 18px;
    height: 18px;
    margin: -4px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
    border-radius: 8px;
}

QSlider::handle:horizontal:hover {
    background: #555555;
    border-color: #666666;
}

QSlider::sub-page:horizontal {
    background: #666666;
    border: 1px solid #999999;
    height: 10px;
    border-radius: 4px;
}
)";


	QVBoxLayout *mainlayout = new QVBoxLayout();

	qlabel_windmill = new QLabel();
	qlabel_windmill->setMovie(qmovie_windmill_gif);

	mainlayout->addWidget(qlabel_windmill);

	qslider_speed_control = new QSlider(Qt::Horizontal);
	qslider_speed_control->setRange(0,100);
	qslider_speed_control->setStyleSheet(sliderStyle);

	mainlayout->addWidget(qslider_speed_control);
	qlabel_speed_display = new QLabel();
	qlabel_speed_display->setText(tr("Geschwindigkeit: 0%"));
	qlabel_speed_display->setStyleSheet(labelStyle);

	mainlayout->addWidget(qlabel_speed_display);

	setLayout(mainlayout);


}

void Windmill::setupStyle()
{

}

void Windmill::setupMedia()
{
	qmovie_windmill_gif = new QMovie(PATH_WINDMILL_GIF);
	
	qmovie_windmill_gif->start();
	qmovie_windmill_gif->stop();
	
}

void Windmill::setupConnection()
{
	connect(qslider_speed_control, SIGNAL(sliderReleased()),
		this, SLOT(speedChangedHelper()));

	connect(qslider_speed_control, SIGNAL(valueChanged(int)),
		this, SLOT(updateSpeedDisplay(int)));
}


void Windmill::resizeEvent(QResizeEvent *ev)
{
	qmovie_windmill_gif->setScaledSize(QSize(qlabel_windmill->width(),qlabel_windmill->height()));
	qmovie_windmill_gif->start();
	qmovie_windmill_gif->stop();
	

}
