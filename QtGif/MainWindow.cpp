#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QWidget(parent)
{
	mainwindow_controller = new MainWindowController(this);

	setupGUI();
	setupSizes();

	connect(qpushbutton_stop_all_trains, SIGNAL(pressed()),
			mainwindow_controller, SLOT(stop_all()));

	connect(qpushbutton_stop_all_trains, SIGNAL(pressed()),
			trainspeed_brown_panel, SLOT(stop()));

	connect(qpushbutton_stop_all_trains, SIGNAL(pressed()),
			trainspeed_yellow_panel, SLOT(stop()));

	connect(qpushbutton_reconnect, SIGNAL(pressed()),
		this, SLOT(setReconnectButton()));

	connect(this, SIGNAL(reconnect()),
		mainwindow_controller, SLOT(reconnect()));

	connect(this, SIGNAL(resetAll()),
		mainwindow_controller, SLOT(resetAll()));
}

void MainWindow::setupGUI()
{

	QString labelTitleStyle = "QLabel { color: #0F172A; text-align: center; font-family: Inter; font-size: 60px; font-style: normal; font-weight: 800;}";
	QString sectionStyle = "QFrame#PanelFrame { "
						   "border: 5px solid #4C4C4C; "
						   "border-radius: 10px; "
						   "}";
	QString b_white = "QFrame#PanelFrame { "
					  "border: 5px solid #4C4C4C; "
					  "border-radius: 10px; "
					  "background-color: #FFF; "
					  "}";
	QString buttonStyleNormal = "QPushButton { background-color: #0F172A; color: white; border: none; border-radius: 8px; padding: 5px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";

	QVBoxLayout *qvbox_screen = new QVBoxLayout();

	QLabel *headerLabel = new QLabel("Modelleisenbahn Steuerung");
	headerLabel->setAlignment(Qt::AlignCenter);
	headerLabel->setStyleSheet(labelTitleStyle);

	QHBoxLayout *qhbox_mainlayout = new QHBoxLayout();

	QVBoxLayout *qvbox_trainselection = new QVBoxLayout();

	QVBoxLayout *qvbox_mainboardselection = new QVBoxLayout();

	QHBoxLayout *qhbox_led_windmill = new QHBoxLayout();

	trainspeed_yellow_panel = new TrainSpeed(this, TrainID::YELLOW);
	trainspeed_brown_panel = new TrainSpeed(this, TrainID::BROWN);

	QFrame *frameForYellowTrain = new QFrame(this);
	frameForYellowTrain->setObjectName("PanelFrame");
	frameForYellowTrain->setStyleSheet(sectionStyle);
	frameForYellowTrain->setLayout(new QVBoxLayout);
	frameForYellowTrain->layout()->addWidget(trainspeed_yellow_panel);

	QFrame *frameForBrownTrain = new QFrame(this);
	frameForBrownTrain->setObjectName("PanelFrame");
	frameForBrownTrain->setStyleSheet(sectionStyle);
	frameForBrownTrain->setLayout(new QVBoxLayout);
	frameForBrownTrain->layout()->addWidget(trainspeed_brown_panel);

	qpushbutton_stop_all_trains = new QPushButton(tr("Alle Züge starten"));
	qpushbutton_stop_all_trains->setStyleSheet(buttonStyleNormal);
	//QIcon playIcon("./res/icon/play.png");
	//qpushbutton_stop_all_trains->setIcon(playIcon);
	//qpushbutton_stop_all_trains->setIconSize(QSize(40, 40));

	qvbox_trainselection->addWidget(frameForYellowTrain);
	qvbox_trainselection->addWidget(frameForBrownTrain);
	qvbox_trainselection->addWidget(qpushbutton_stop_all_trains);

	qpushbutton_reconnect = new QPushButton(tr("Neu Verbinden"));
	qpushbutton_reconnect->setStyleSheet(buttonStyleNormal);

	
	
	changetrack_panel = new ChangeTrack(this);
	ledselection_panel = new LedSelection(this);
	windmill_panel = new Windmill(this);

	QFrame *frameForChangeTrack = new QFrame(this);
	frameForChangeTrack->setObjectName("PanelFrame");
	frameForChangeTrack->setStyleSheet(sectionStyle);
	frameForChangeTrack->setLayout(new QVBoxLayout);
	changetrack_panel->setAlignment(Qt::AlignCenter);
	frameForChangeTrack->layout()->addWidget(changetrack_panel);

	QFrame *frameForLedSelection = new QFrame(this);
	frameForLedSelection->setObjectName("PanelFrame");
	frameForLedSelection->setStyleSheet(sectionStyle);
	frameForLedSelection->setLayout(new QVBoxLayout);
	frameForLedSelection->layout()->addWidget(ledselection_panel);

	QFrame *frameForWindmill = new QFrame(this);
	frameForWindmill->setObjectName("PanelFrame");
	frameForWindmill->setStyleSheet(sectionStyle + b_white);
	frameForWindmill->setLayout(new QVBoxLayout);
	frameForWindmill->layout()->addWidget(windmill_panel);

	qvbox_mainboardselection->addWidget(frameForChangeTrack);

	qhbox_led_windmill->addWidget(frameForLedSelection);
	qhbox_led_windmill->addWidget(frameForWindmill);
	qvbox_mainboardselection->addLayout(qhbox_led_windmill);

	qvbox_mainboardselection->addLayout(qhbox_led_windmill);

	qhbox_mainlayout->addLayout(qvbox_trainselection);
	qhbox_mainlayout->addLayout(qvbox_mainboardselection);

	qvbox_screen->addWidget(headerLabel);

	QHBoxLayout *qhbox_reconnect = new QHBoxLayout();
	qhbox_reconnect->addStretch();
	qhbox_reconnect->addWidget(qpushbutton_reconnect);

	qvbox_screen->addLayout(qhbox_reconnect);

	qvbox_screen->addLayout(qhbox_mainlayout);

	setLayout(qvbox_screen);
}

void MainWindow::setupSizes()
{
	windmill_panel->setMaximumSize(QSize(300, 400));
	// ledselection_panel->setMaximumSize(QSize(400,400));
	// ledselection_panel->setMinimumSize(QSize(400,400));
}

void MainWindow::updateReconnect()
{

	QString buttonStyleNormal = "QPushButton { background-color: #0F172A; color: white; border: none; border-radius: 8px; padding: 5px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";


	qpushbutton_reconnect->setStyleSheet(buttonStyleNormal);


	qpushbutton_reconnect->setText("Neu Verbinden");

	qpushbutton_reconnect->update();

}


void MainWindow::closeEvent(QCloseEvent * ev)
{
	emit resetAll();
}


void MainWindow::setReconnectButton()
{
	QString buttonStyleNormal = "QPushButton { background-color: #333; color: white; border: none; border-radius: 8px; padding: 5px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";


	qpushbutton_reconnect->setStyleSheet(buttonStyleNormal);

	qpushbutton_reconnect->setText("Warten...");
	qpushbutton_reconnect->update();

	emit reconnect();
}
