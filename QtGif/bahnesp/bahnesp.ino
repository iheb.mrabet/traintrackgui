#include <Arduino.h>
//#include "all_includes.h"

//#include "tc.h"
#include "hw-ports.h"
#include "sw-ports.h"
#include "io-board.h"
#include "sp.h"
#include "tics.h"
#include "stics.h"
#include "ramp.h"
#include "emk-adc.h"
#include "pwm.h"
#include "usr.h"
#include "pid-default.h"
#include "tc.h"
#include "train-config.h"
#define ESP32=1
#define TRAIN=1
#include "winterface.h"
#include "dr.h"
#include "led.h"
#include "weiche.h"
#include <WiFi.h>


const char* ssid = "Pifi";
const char* password = "beerebeere";


char debug_buf[1024] ={0};
volatile int buf_set = 0;

LD* led;
SP *weichen[2];

//#define ADC_MAX 4096
//#define ADC_REF 3.3
//#define P_GAIN 400.0
//#define I_GAIN 5
#define TIC_MAX 256
#define TIC_INTERVAL 500

#define PIN_H1 26
#define PIN_H2 25
#define PIN_IDENTIFIER 33

void hi(){
//	Serial.println("Hi");
}

//volatile int arr_from = 0;
//int arr_size = 500;
#define PIN_TEST 23

void IRAM_ATTR interrupt()
{
	swp_handlers();
}
volatile double g_soll=0.5;
hw_timer_t *tic_timer = NULL;

WINT net;
WINTP netw = &net;
CMD cmd_arr[CMD_ARR_ENTRYS];

//MC aMc;
//MCP mc = &aMc;
PWM *pwm;
EMK *emk;

DR drs;
DR* dr= &drs;

SP* dr_0;
SP* dr_1;

TC train;
TC *tcp = &train;

void setup_wifi( int port )
{
	int ip;
	if( port == TRAIN_PORT_Y ){
		ip = 17;	
	}else{
		ip = 18;	
	}
		IPAddress local_IP(192, 168, 2, ip);
		IPAddress gateway(192, 168, 2, 1);
		IPAddress subnet(255, 255, 255, 0);
		IPAddress primaryDNS(192, 168, 2, 1); // Not Mandatory
		IPAddress secondaryDNS(0, 0, 0, 0);     // Not Mandatory




	if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    	Serial.println("STA Failed to configure");
  }	


	WiFi.begin(ssid, password);
    Serial.println("\nConnecting");

    while(WiFi.status() != WL_CONNECTED){
        Serial.print(".");
        delay(100);
    }

    Serial.println("\nConnected to the WiFi network");
    Serial.print("Local ESP32 IP: ");
    Serial.println(WiFi.localIP());
}

int port;  //Port number
WiFiServer server_y( TRAIN_PORT_Y );
WiFiServer server_b( TRAIN_PORT_B );
WiFiClient client; //= server.available();

void setup()
{
	Serial.begin(115200);

	Serial.printf("iob config...\n");
	iob_init();
	Serial.printf("pwm config...\n");
        pwm = (PWM *) iob_portconfig( 0, HWP_BIN_OUT );	// port 0
	pwm_config( pwm );			// port nr; has to be used twice
	pwm_start( pwm );			// go
	
	Serial.printf("emk config...\n");
	emk = (EMK *) iob_portconfig( 1, HWP_INT_IN );	// reading ADC values
	emk_config( emk, 0 );
	conf_emk( emk, TENDER );

	Serial.printf("tc config...\n");

	dr_0 = (SP*) iob_portconfig( 2, HWP_BIN_OUT );
	dr_1 = (SP*) iob_portconfig( 3, HWP_BIN_OUT );
	dr_config(dr, dr_0, dr_1 );
	
	Serial.printf("dir config...\n");



	tc_config( tcp, pwm, emk );
	


	Serial.println(" What nu ?");
	Serial.printf("pid config...\n");
	conf_pidstd( (PID_STD*)tcp->pid_data, TENDER_STD );

	Serial.printf("Wifi...\n");
	
	pinMode( PIN_IDENTIFIER, INPUT	);
	if( digitalRead( PIN_IDENTIFIER ) == HIGH ){
		port = TRAIN_PORT_B;
	}else{
		port = TRAIN_PORT_Y;
	}
	setup_wifi(port);
  	server_y.begin();
  	server_b.begin();
	cmd_arr_init( cmd_arr );
    	pinMode(LED_BUILTIN, OUTPUT);
	Serial.println("Cmd arr init survived!");

	Serial.println("Setup survived!");
	tic_timer = timerBegin(0, 80, true);
   	timerAttachInterrupt(tic_timer, interrupt, true);
   	timerAlarmWrite(tic_timer, TIC_INTERVAL, true);
   	timerAlarmEnable(tic_timer);

	Serial.println("Tic creation survived! \n");


//	tc_settarget(tcp, 0);
/*
	print_tab();
	Serial.printf(" pwm: %p	\n",pwm_state);
	Serial.printf(" emk: %p	\n",emk_state);
	Serial.printf(" usr: %p	\n",usr_state);
	Serial.printf(" dummy: %p	\n",iob_dummy);
*/
}

void loop()
{
/*
static int cnt = 0;
 cnt++;
 if( cnt == 10000 ){
 	Serial.printf("%d \n",tcp->target_speed);
	cnt = 0;
 }
*/
  
  delay(1);
	if( port == TRAIN_PORT_Y){
	  client = server_y.available();
//	Serial.print(port);
	}
	else{
	  client = server_b.available();
//	Serial.print(port);
	}
  if (client) {
    if(client.connected())
    {
      Serial.println("Client Connected");
	netw_init(netw, 0);
	netw->client = client;     
    	while(netw->client.connected()){      

//		Serial.println( client.available() );
		while( client.available() > 0 ){
				netw_recv( netw );
//				Serial.println("Receive done\n");
//				netw_analyze_payload( netw );
//				netw_analyze_arg( netw );
				netw_handle_message( netw, cmd_arr );
				netw_clear_arg( netw );
				netw_clear_buf( netw );
//				Serial.println("Buf clear started");
//				while( client.available() > 0)
//					client.read();	
//				Serial.println("Buf cleared...");	
		}
    }
    }
    client.stop();
    Serial.println("Client disconnected");    
  }

// if ( buf_set )
// {
//	Serial.printf("%s",debug_buf);
//	buf_set = 0;
//} 
/*
	// Quick'n'Dirty
  char input[64]={0};
  if (Serial.available() > 0) {
    int cursor = 0;
    int byte=51;
    while( cursor < 63  && byte != '\n') 
    {
      byte = Serial.read();
      input[cursor] = byte;
      cursor++;
    
       Serial.print("I received: ");
       Serial.println(byte);
    }
    input[cursor]='\0';
//    g_soll = atof( input );
	tc_speedset( atoi(input) );
	Serial.printf("Ok ?\n");
//    Serial.printf("global_speed: %f %s\n",g_soll,input);
    input[0]='\0';
  }
*/
}


