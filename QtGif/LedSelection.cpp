#include "LedSelection.h"






LedSelection::LedSelection(QWidget * parent)
: QWidget(parent)
{

	QString buttonStyleRound = "QPushButton { background-color: white; color: black; border: 1px; border-radius: 25px; padding: 5px; font-family: Inter; font-size: 30px; font-style: normal; font-weight: 600;}";
    QString labelStyle = "QLabel { color: #0F172A; font-family : Inter; font-size : 25px; font-style : normal; font-weight : 800;}";


	ledselectioncontroller = new LedSelectionController(this);
		

	double a =1.0;

    	QList<LED> list_big_house = {
    				       LED(BIG_HOUSE_TOP,0.28,0.3,a),

    				       LED(BIG_HOUSE_DOWN,0.2,0.75,a),

    				       LED(BIG_HOUSE_SIDE,0.7,0.75,a),  
    				       };
    	
  
   	QList<LED> list_small_house = {
    				     LED(SMALL_HOUSE,0.445,0.4,a),
    				     };



	selection_big_house = new Selection(this, 
					    PATH_BIG_HOUSE,
					    PATH_LIGHT_ON,
					    PATH_LIGHT_OFF,
					    std::move(list_big_house)
					    );
	
	selection_small_house = new Selection(this, 
					    PATH_SMALL_HOUSE,
					    PATH_LIGHT_ON,
					    PATH_LIGHT_OFF,
					    std::move(list_small_house)
					    );
	
	
	// SETTMINIMUM ------------------------------------------------------------------------------
	// SETTMINIMUM ------------------------------------------------------------------------------
	// SETTMINIMUM ------------------------------------------------------------------------------
	selection_small_house->setMinimumSize(QSize(350,200));
	selection_big_house->setMinimumSize(QSize(535,300));
	// SETTMINIMUM ------------------------------------------------------------------------------
	// SETTMINIMUM ------------------------------------------------------------------------------
	// SETTMINIMUM ------------------------------------------------------------------------------

	QPixmap light_bulb_on(PATH_LIGHT_ON);
	QPixmap light_bulb_off(PATH_LIGHT_OFF);


	QHBoxLayout *qpushbutton_all_on_Layout = new QHBoxLayout();
	QHBoxLayout *qpushbutton_all_off_Layout = new QHBoxLayout();

	QLabel *qpushbutton_all_on_label = new QLabel("Alle Lichter an");
	QLabel *qpushbutton_all_off_label = new QLabel("Alle Lichter aus");
	qpushbutton_all_on_label->setStyleSheet(labelStyle);
	qpushbutton_all_off_label->setStyleSheet(labelStyle);

	qpushbutton_all_on = new QPushButton(QIcon(light_bulb_on), tr(""));
	qpushbutton_all_off = new QPushButton(QIcon(light_bulb_off), tr(""));
    qpushbutton_all_on->setStyleSheet(buttonStyleRound);
    qpushbutton_all_off->setStyleSheet(buttonStyleRound);

	qpushbutton_all_on->setMinimumHeight(40);
	qpushbutton_all_on->setIconSize(QSize(40,40));
	qpushbutton_all_off->setMinimumHeight(40);
	qpushbutton_all_off->setIconSize(QSize(40,40));

	QVBoxLayout *mainlayout = new QVBoxLayout();
	
	mainlayout->addWidget(selection_big_house);

	QHBoxLayout *small_layout = new QHBoxLayout();

	small_layout->addWidget(selection_small_house);
	
	QVBoxLayout *button_layout = new QVBoxLayout();


	qpushbutton_all_on_Layout->addWidget(qpushbutton_all_on_label);	
	qpushbutton_all_off_Layout->addWidget(qpushbutton_all_off_label);	
	qpushbutton_all_on_Layout->addWidget(qpushbutton_all_on);	
	qpushbutton_all_off_Layout->addWidget(qpushbutton_all_off);	

	button_layout->addLayout(qpushbutton_all_on_Layout);	
	button_layout->addLayout(qpushbutton_all_off_Layout);	

	small_layout->addLayout(button_layout);

	mainlayout->addLayout(small_layout);



	connect(qpushbutton_all_off, SIGNAL(pressed()),
		selection_big_house, SLOT(everything_off()));

	connect(qpushbutton_all_off, SIGNAL(pressed()),
		selection_small_house, SLOT(everything_off()));

	connect(qpushbutton_all_on, SIGNAL(pressed()),
		selection_big_house, SLOT(everything_on()));

	connect(qpushbutton_all_on, SIGNAL(pressed()),
		selection_small_house, SLOT(everything_on()));


	setLayout(mainlayout);
}







Selection::Selection(QWidget* parent,
		     const QString &path_house,
	             const QString& path_led_on,
		     const QString& path_led_off,
		     QList<LED> &&list)
: QLabel(parent)
{
	selectioncontroller = new SelectionController(this);
	
	qpixmap_house = new QPixmap(path_house);
	qpixmap_led_on = new QPixmap(path_led_on);
	qpixmap_led_off = new QPixmap(path_led_off);
	qpixmap_result = new QPixmap(*qpixmap_house);

	qlist_leds = list;

	for(LED& leds : qlist_leds)
	{
		leds.setReferenceImage(qpixmap_house->width(), qpixmap_house->height());
		leds.setOriginal(qpixmap_house->width(), qpixmap_house->height());
		leds.setupTextures(qpixmap_led_on, qpixmap_led_off);
		leds.setOn(false);
	}

	drawResultImage();
	
}



void Selection::drawResultImage()
{
	
	*qpixmap_result = *qpixmap_house;

	*qpixmap_result = qpixmap_result->scaled(width(), height());	
	QPainter paint(qpixmap_result);


	for(LED& led : qlist_leds)
	{
		paint.drawPixmap(led.getPos(), led.getScaledImage());
	}

	setPixmap(*qpixmap_result);
	update();
}

void Selection::resizeEvent(QResizeEvent *ev)
{
	QSize scale(ev->size().width()/ev->oldSize().width(),
		    ev->size().height()/ev->oldSize().height());


	for(LED& led : qlist_leds)
	{
		led.setReferenceImage(width(), height());
	}

	drawResultImage();

}


void Selection::mousePressEvent(QMouseEvent *ev)
{
	int x = ev->x();
	int y = ev->y();

	for(LED& led : qlist_leds)
	{
		if(led.intersect(QPoint(x,y)))
		{
			LED_CODE(led.LEDid, !led.getOn());
			led.toggle();
		}	
	}	
	drawResultImage();
	

}


void Selection::everything_on()
{
	for(LED& led : qlist_leds)
	{
		led.setOn(true);
		LED_CODE(led.LEDid,true);
	}
	drawResultImage();
}

void Selection::everything_off()
{
	for(LED& led : qlist_leds)
	{
		led.setOn(false);
		LED_CODE(led.LEDid,false);
	}
	drawResultImage();
}

void Selection::LED_CODE(int code, bool on)
{
	switch(code)
	{
		case BIG_HOUSE_TOP:
			
			emit setLed(7, on);
			emit setLed(6, on);
			emit setLed(5, on);
			emit setLed(9, on);
			break;
		case BIG_HOUSE_DOWN:

			emit setLed(10, on);
			emit setLed(8, on);
			break;
		case BIG_HOUSE_SIDE:

			emit setLed(4, on);
			emit setLed(3, on);
			break;

		case SMALL_HOUSE:


			emit setLed(0, on);
			emit setLed(1, on);
			emit setLed(2, on);
			break;

	}
}
