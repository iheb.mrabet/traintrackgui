#include "TrainSpeedController.h"
#include "Connector.h"

TrainSpeedController::TrainSpeedController(QObject * view, TrainID id)
: train_id(id)
{
    current_direction = DIRECTION::forward;
    current_speed = 0;    
    train_running = false;

    connect(this, SIGNAL(updateDirection(int)),
            view, SLOT(updateDirection(int)));

    connect(this, SIGNAL(updateSpeedDisplay(int)),
            view, SLOT(updateSpeedDisplay(int)));

    connect(this, SIGNAL(updateStartStopButton(bool)),
            view, SLOT(updateStartStopButton(bool)));

}

void TrainSpeedController::change_speed(int a)
{
    current_speed = a;
    if(train_running)
    {        
        int tmp = double(current_speed)/100.0 * MAXIMUM_SPEED;
        API->setSpeed(train_id,tmp);
        emit updateStartStopButton(false); 
    }

    emit updateSpeedDisplay(a);
}

void TrainSpeedController::change_direction()
{
    switch(current_direction)
    {
        case DIRECTION::forward:
            current_direction = DIRECTION::backward;
            break;
        case DIRECTION::backward:
            current_direction = DIRECTION::forward;
            break;
    }

    API->setDirection(train_id,current_direction);

    emit updateDirection(current_direction);
}

void TrainSpeedController::start_stop()
{
    if(!train_running)
    {        
        train_running = true;
        int tmp = double(current_speed)/100.0 * MAXIMUM_SPEED;
        API->setSpeed(train_id,tmp);
        emit updateStartStopButton(false); 
    }
    else
    {
        train_running = false;
        API->setSpeed(train_id, 0);
        emit updateStartStopButton(true); 
    }
        
}

void TrainSpeedController::stop()
{
	train_running = false;
	API->setSpeed(train_id, 0);	
	emit updateStartStopButton(true);
}
