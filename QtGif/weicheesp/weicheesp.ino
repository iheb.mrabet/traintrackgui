

//#include <stdio.h>
//#include <stdlib.h>

#define MAINBOARD

#include "hw-ports.h"
#include "sw-ports.h"
#include "io-board.h"

#include "tics.h"
#include "stics.h"

#include "pwm.h"
#include "mf.h"
#include "weiche.h"
#include "sp.h"
#include <WiFi.h>
#include "winterface.h"
#include "Adafruit_TLC5947.h"
#include "led.h"

#define TIC_MAX 256
#define TIC_INTERVAL 50000
// mf and pwm might also be global variables.....

#define DATA_PIN  14   //legt pin für data fest( GPIO4)
#define CLOCK_PIN   12  //legt pin für clock fest(GPIO5)
#define LATCH_PIN   13  // legt pin für latch fest(GPIO12)
#define oe  -1  // auf -1 setzen, um den Enable-Pin nicht zu verwenden (optional) 


const char* ssid = "Pifi";
const char* password = "beerebeere";
int port = 9153;  //Port number
WiFiServer server(port);
WiFiClient client; //= server.available();

WINT net;
WINTP netw = &net;

Adafruit_TLC5947 *tlc ; 


LD _led;
LD *led = &_led;

CMD cmd_arr[CMD_ARR_ENTRYS];

hw_timer_t *tic_timer = NULL;
SP *weichen[2];


void dynamic_reconf()
     {
	MF  *mf;
        mf = (MF *) iob_portconfig( 0, HWP_BIN_OUT );
	mf_config( mf, 0 );
	mf_set( mf, 2 );
	mf_trigger( mf );
     }

void IRAM_ATTR interrupt()
{
	swp_handlers();
}

void setup_wifi()
{
	WiFi.begin(ssid, password);
    Serial.println("\nConnecting");

    while(WiFi.status() != WL_CONNECTED){
        Serial.print(".");
        delay(100);
    }


    Serial.println("\nConnected to the WiFi network");
    Serial.print("Local ESP32 IP: ");
    Serial.println(WiFi.localIP());
}


void setup()
     {
	Serial.begin(115000);
	PWM *pwm;
	MF  *mf;
	// initialize
	iob_init();
	// setting up port 0 for pwm
		
	tlc = new Adafruit_TLC5947(1, CLOCK_PIN, DATA_PIN, LATCH_PIN);
	Serial.printf(" %d \n", tlc->getPWM(0));
	tlc->begin();
	led_config( led, tlc );	
	led_set( led, 0, 0 );
	led_state(led );

	
	weichen[0] = (SP*) iob_portconfig( 0 , HWP_BIN_OUT );
	weichen[1] = (SP*) iob_portconfig( 1 , HWP_BIN_OUT );
	sp_config( weichen[0],0 );
	sp_config( weichen[1],1 );
//	swp_register( sp_state, weichen[0] );
  //      swp_register( sp_state, weichen[1] );


	setup_wifi();
  	server.begin();
	cmd_arr_init( cmd_arr );
    	pinMode(LED_BUILTIN, OUTPUT);
	Serial.println("Cmd arr init survived!");


	tic_timer = timerBegin(0, 80, true);
   	timerAttachInterrupt(tic_timer, interrupt, true);
   	timerAlarmWrite(tic_timer, TIC_INTERVAL, true);
   	//timerAlarmDisable(tic_timer);
   	timerAlarmEnable(tic_timer);
}

void loop(){

  if (client) {
	netw_init(netw, 0);
	netw->client = client;     
    if(client.connected()){
     	Serial.println("Client Connected");
    	while(netw->client.connected()){      
//			Serial.println( client.available() );
			while( client.available() > 0 ){
				netw_recv( netw );
//				Serial.println("Receive done\n");
//				netw_analyze_payload( netw );
//				netw_analyze_arg( netw );
				netw_handle_message( netw, cmd_arr );
				netw_clear_arg( netw );
				netw_clear_buf( netw );
//				Serial.println("Buf clear started");
	//				while( client.available() > 0)
	//					client.read();	
//				Serial.println("Buf cleared...");	
			}
    	}
    	client.stop();
    	}
	}else{
  		client = server.available();
	}
//    Serial.println("Client disconnected");    
}

/*	weiche_set( weichen, 0 , 1 );
	weiche_set( weichen, 1 , 1 );
	delay(2000);
	weiche_set( weichen, 0 , 0 );
	weiche_set( weichen, 1 , 0 );

	delay(2000);
*/



