#include <QApplication>
#include <QList>
#include "TrainSpeed.h"

#include "MainWindow.h"

int main(int argc, char ** argv)
{
    QApplication app(argc,argv);

    MainWindow window;

    window.show();
    
    return app.exec();
}
