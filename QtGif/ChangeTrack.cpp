#include "ChangeTrack.h"


ChangeTrack::ChangeTrack(QWidget * parent)
: QLabel(parent)
{


	setupMedia();
	changetrackcontroller = new ChangeTrackController(this);

	setupConnection();
}



void ChangeTrack::TrackChoosen(int track)
{
	switch(track)
	{
	case TRACK::INNER:
			  this->setPixmap(*qpixmap_inner_track);
			  break;
	case TRACK::OUTER:
			  this->setPixmap(*qpixmap_outer_track);
			  break;
			
	}	
	
}

void ChangeTrack::mousePressEvent(QMouseEvent *event)
{
	emit TrackClicked();
}


void ChangeTrack::setupMedia()
{
	qpixmap_outer_track = new QPixmap();
	qpixmap_outer_track->load(PATH_OUTER_TRACK);
	*qpixmap_outer_track = qpixmap_outer_track->scaled(400,300); 

	qpixmap_inner_track = new QPixmap();
	qpixmap_inner_track->load(PATH_INNER_TRACK);
	*qpixmap_inner_track = qpixmap_inner_track->scaled(400,300); 

}

void ChangeTrack::setupConnection()
{
}

void ChangeTrack::setupStyle()
{

}
