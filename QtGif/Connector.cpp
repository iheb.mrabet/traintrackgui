#include "Connector.h"

#include "Logger.h"

//#define GUI_TEST

#ifndef GUI_TEST

bool Connector::connect()
{

    Zug[0] = connection_init(TRAIN_Y , TRAIN_PORT_Y );
	Zug[1] = connection_init(TRAIN_B , TRAIN_PORT_B );
	mainboard = connection_init( MB, MB_PORT );

    return true;
}

bool Connector::connectDummy()
{
    Zug[0] = connection_init( DUMMY_Y, TRAIN_PORT_Y );
	Zug[1] = connection_init( DUMMY_B, TRAIN_PORT_B );
	mainboard = connection_init( DUMMY_MB, MB_PORT );

    return true;
}


            

bool Connector::setLed(int led, bool set)
{
	
    return set ? mainboard_setLed(mainboard, led, 4095) : mainboard_setLed(mainboard, led, 0);

}

bool Connector::flushLed()
{

    return mainboard_writeLed(mainboard);
}

bool Connector::setSpeed(TrainID id, int speed) //speed = [0,100]
{

    
    return train_setSpeed(Zug[id], speed);
    
}

bool Connector::setDirection(TrainID id, int direction) //direction = [-1,1] = [vorwaerts, rueckwaerts]
{
	

    return train_setDirection(Zug[id], direction);
}

bool Connector::setWeiche(int Weiche, bool direction)
{               


    return mainboard_setWeiche(mainboard, Weiche, direction);
}

bool Connector::setTrack(int id) // id = [0,1] -> [kleiner Track, großer Track]
{



    bool ret = true;

    switch(id)
    {
        case TRACK1:
            ret &= mainboard_setWeiche(mainboard, 0, true);
            ret &= mainboard_setWeiche(mainboard, 1, true);
            return ret;
        
        case TRACK2:
            ret &= mainboard_setWeiche(mainboard, 0, false);
            ret &= mainboard_setWeiche(mainboard, 1, false);
            return ret;
    }           

    return !ret;
}

bool Connector::setWindradSpeed(int speed) // speed = [0,100]
{
    int result_speed = 4095.0 * (double(speed) / 100.0);
    mainboard_setLed(mainboard, WINDMILL, result_speed);
    return flushLed(); 
}

bool Connector::resetLED()
{	
	for(int i = 0; i < NUMBER_OF_LEDS; i++)
	{
		setLed(i, false);
	}	
	flushLed();
}

#else

bool Connector::connect()
{


    return true;
}

bool Connector::connectDummy()
{

    return true;
}


            

bool Connector::setLed(int led, bool set)
{
	

}

bool Connector::flushLed()
{

}

bool Connector::setSpeed(TrainID id, int speed) //speed = [0,100]
{

    
    
}

bool Connector::setDirection(TrainID id, int direction) //direction = [-1,1] = [vorwaerts, rueckwaerts]
{
	

}

bool Connector::setWeiche(int Weiche, bool direction)
{               


}

bool Connector::setTrack(int id) // id = [0,1] -> [kleiner Track, großer Track]
{



}

bool Connector::setWindradSpeed(int speed) // speed = [0,100]
{
}

bool Connector::resetLED()
{	

}

#endif // GUI_TEST

Connector * Connector::_API = 0;
