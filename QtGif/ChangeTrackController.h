#ifndef CHANGETRACKCONTROLLER_H
#define CHANGETRACKCONTROLLER_H

#include <QObject>

#include "Connector.h"

enum TRACK{ INNER = TRACK1, OUTER = TRACK2};

class ChangeTrackController : public QObject
{

	Q_OBJECT
	public:

		ChangeTrackController(QObject *view);
		void setTrack(TRACK track);	
	signals:
		
		void TrackChoosen(int);

	public slots:
		void TrackClicked(); 

	private:
	
		TRACK currentTrack;	

};

#endif //CHANGETRACKCONTROLLER_H
