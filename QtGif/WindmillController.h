#ifndef WINDMILLCONTROLLER_H
#define WINDMILLCONTROLLER_H


#include <QObject>
#include "Connector.h"


#define WINDMILL_THRESHOLD 20

class WindmillController : public QObject
{

	Q_OBJECT
	
	public:
		
		WindmillController(QObject * view);
	
	
	signals:

		
	
	public slots:

		void speedChanged(int);
	
	private:
		
		int current_speed = 0;		
	

};



#endif //WINDMILLCONTROLLER_H

