#ifndef LEDSELECTIONCONTROLLER_H
#define LEDSELECTIONCONTROLLER_H

#include <QObject>

#include "Connector.h"

class LedSelectionController : public QObject
{
	Q_OBJECT

	public:
		LedSelectionController(QObject *view);

	signals:

	public slots:


	private:

};


class SelectionController : public QObject
{
	Q_OBJECT
	
	public:
		SelectionController(QObject *view);

	signals:

	public slots:
		
		void setLed(int,bool);	

	private:

	
};


#endif //LEDSELECTIONCONTROLLER_H

