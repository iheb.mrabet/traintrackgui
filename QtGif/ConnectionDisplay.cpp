#include "ConnectionDisplay.h"



ConnectionDisplay::ConnectionDisplay(QWidget * parent, TrainID id) :
QLabel(parent)
{
    qtimer_5sec = new QTimer(this);


    connectiondisplay_controller = new ConnectionDisplayController(this,id);    
    connect(qtimer_5sec, SIGNAL(timeout()),
            connectiondisplay_controller, SLOT(getConnectionStatus()));

    qpixmap_connection_online = new QPixmap(PATH_CONNECTION_ONLINE);
    qpixmap_connection_offline = new QPixmap(PATH_CONNECTION_OFFLINE);
    this->setPixmap(*qpixmap_connection_offline);

}


void ConnectionDisplay::updateConnectionDisplay(bool connection_successful)
{
    if(connection_successful)
        this->setPixmap(*qpixmap_connection_online);
    else
        this->setPixmap(*qpixmap_connection_offline);

}
