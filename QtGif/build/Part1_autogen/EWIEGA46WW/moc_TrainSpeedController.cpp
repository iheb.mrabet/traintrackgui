/****************************************************************************
** Meta object code from reading C++ file 'TrainSpeedController.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.2.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../TrainSpeedController.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrainSpeedController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.2.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TrainSpeedController_t {
    const uint offsetsAndSize[18];
    char stringdata0[125];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_TrainSpeedController_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_TrainSpeedController_t qt_meta_stringdata_TrainSpeedController = {
    {
QT_MOC_LITERAL(0, 20), // "TrainSpeedController"
QT_MOC_LITERAL(21, 15), // "updateDirection"
QT_MOC_LITERAL(37, 0), // ""
QT_MOC_LITERAL(38, 18), // "updateSpeedDisplay"
QT_MOC_LITERAL(57, 21), // "updateStartStopButton"
QT_MOC_LITERAL(79, 12), // "change_speed"
QT_MOC_LITERAL(92, 16), // "change_direction"
QT_MOC_LITERAL(109, 10), // "start_stop"
QT_MOC_LITERAL(120, 4) // "stop"

    },
    "TrainSpeedController\0updateDirection\0"
    "\0updateSpeedDisplay\0updateStartStopButton\0"
    "change_speed\0change_direction\0start_stop\0"
    "stop"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrainSpeedController[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,   56,    2, 0x06,    1 /* Public */,
       3,    1,   59,    2, 0x06,    3 /* Public */,
       4,    1,   62,    2, 0x06,    5 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       5,    1,   65,    2, 0x0a,    7 /* Public */,
       6,    0,   68,    2, 0x0a,    9 /* Public */,
       7,    0,   69,    2, 0x0a,   10 /* Public */,
       8,    0,   70,    2, 0x0a,   11 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TrainSpeedController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TrainSpeedController *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->updateDirection((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 1: _t->updateSpeedDisplay((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 2: _t->updateStartStopButton((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 3: _t->change_speed((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 4: _t->change_direction(); break;
        case 5: _t->start_stop(); break;
        case 6: _t->stop(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TrainSpeedController::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TrainSpeedController::updateDirection)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TrainSpeedController::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TrainSpeedController::updateSpeedDisplay)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TrainSpeedController::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TrainSpeedController::updateStartStopButton)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject TrainSpeedController::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_TrainSpeedController.offsetsAndSize,
    qt_meta_data_TrainSpeedController,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_TrainSpeedController_t
, QtPrivate::TypeAndForceComplete<TrainSpeedController, std::true_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *TrainSpeedController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrainSpeedController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrainSpeedController.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TrainSpeedController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void TrainSpeedController::updateDirection(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TrainSpeedController::updateSpeedDisplay(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TrainSpeedController::updateStartStopButton(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
