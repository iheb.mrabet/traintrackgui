#include "TestGui.h"
#include <QGridLayout>


TestGui::TestGui()
: QWidget(nullptr)
{
    QGridLayout *layout = new QGridLayout(this);
    

    combo_box_widget_selecetion = new QComboBox(this);
    layout->addWidget(combo_box_widget_selecetion,0,0,1,2);




    selectionWidget = new QStackedWidget(this);
    layout->addWidget(selectionWidget,1,0,3,2);

    placeHolder = new QWidget(this);
    placeHolder->setMinimumSize(QSize(400,400));


    connect(combo_box_widget_selecetion, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setWidgetAccordingToSelection(const QString&))); 

    TestSpeed * speed = new TestSpeed(this);
    TestLed *led = new TestLed(this);
    TestWindMill *windmill = new TestWindMill(this);
    TestWeiche *weiche = new TestWeiche(this);

    addWidget(placeHolder, tr("Nothing"));
    addWidget(speed, tr("Test Speed"));
    addWidget(led,tr("Test Led"));
    addWidget(windmill,tr("Test Windmill"));
    addWidget(weiche, tr("Test Weiche"));

    // API SetUP

    API->connectDummy();

    
    setLayout(layout);    
}



void TestGui::addWidget(QWidget* widget, const QString &name)
{
    widgets_collection[name] = widget;
    selectionWidget->addWidget(widget);
    combo_box_widget_selecetion->addItem(name);
     
}

void TestGui::setWidgetAccordingToSelection(const QString& text)
{   
    if(QWidget* selectedWidget = widgets_collection[text])
    {
	    selectionWidget->setCurrentWidget(selectedWidget);
    }
}
