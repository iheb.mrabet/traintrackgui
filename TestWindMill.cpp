#include "TestWindMill.h"



TestWindMill::TestWindMill(QWidget * parent):
QWidget(parent)
{
	QVBoxLayout * layout = new QVBoxLayout;

	speed_display = new QLabel(tr("Speed: 0%"));

	speed_sld = new QSlider(Qt::Horizontal);
	speed_sld->setRange(0,100);


	layout->addWidget(speed_display);
	layout->addWidget(speed_sld);
	layout->addStretch();

	connect(speed_sld, SIGNAL(sliderReleased()),this,SLOT(SpeedChanged()));
	connect(speed_sld, SIGNAL(valueChanged(int)),this,SLOT(DisplayChanged(int)));

	setLayout(layout);
}

void TestWindMill::SpeedChanged()
{
	int speed = speed_sld->value();
	if(speed < WINDMILL_THRESHOLD && current_speed < 3)
	{
		//Windmill won't start at low rpm
		//Let it first reach its threshold rpm
		//and then go down.

		API->setWindradSpeed(WINDMILL_THRESHOLD);
		speed_sld->setDisabled(true);
		sleep(0.5);
		speed_sld->setDisabled(false);
	}

	API->setWindradSpeed(speed);


	current_speed = speed;
}

void TestWindMill::DisplayChanged(int speed)
{

	QString tmp;
	QTextStream(&tmp) << "Speed: "<<speed<<"%";
	speed_display->setText(tmp);
}
