#include "TestWeiche.h"


TestWeiche::TestWeiche(QWidget * parent):
QWidget(parent)
{
	QHBoxLayout *layout = new QHBoxLayout;

	QPushButton * track1_btn = new QPushButton(tr("Track 1"));
	QPushButton * track2_btn = new QPushButton(tr("Track 2"));

	layout->addWidget(track1_btn);
	layout->addWidget(track2_btn);

	connect(track1_btn,SIGNAL(pressed()),this,SLOT(track1choosen()));
	connect(track2_btn,SIGNAL(pressed()),this,SLOT(track2choosen()));

	setLayout(layout);
}


void TestWeiche::track1choosen()
{
	API->setTrack(TRACK1);
	sleep(1);
}


void TestWeiche::track2choosen()
{

	API->setTrack(TRACK2);
	sleep(1);
}


