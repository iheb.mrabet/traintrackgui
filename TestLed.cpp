#include "TestLed.h"



TestLed::TestLed(QWidget*parent) : 
QWidget(parent)
{

	QVBoxLayout *mainlayout = new QVBoxLayout();

 	//----------------------------------Kleines Haus-------------------------------------
	
	{
		QGroupBox* kleines_haus = new QGroupBox(tr("Kleines Haus"), this);

		mainlayout->addWidget(kleines_haus);

	

		QVBoxLayout *layout_klein = new QVBoxLayout();
	
		QGroupBox* klein_haus_erste_etage = new QGroupBox(tr("1. Etage"),kleines_haus);

		kleines_haus->setLayout(layout_klein);

		QHBoxLayout *layout_kl_1_etage = new QHBoxLayout;

		

		QPushButton *btn1 = new QPushButton();
		button_to_led[btn1] = 2;
		button_toggle[btn1] = false;

		layout_kl_1_etage->addWidget(btn1);
		connect(btn1,SIGNAL(pressed()),this,SLOT(btn_pressed()));

		QPushButton *btn2 = new QPushButton();
		button_to_led[btn2] = 0;
		button_toggle[btn2] = false;

		layout_kl_1_etage->addWidget(btn2);
		connect(btn2,SIGNAL(pressed()),this,SLOT(btn_pressed()));
		
		klein_haus_erste_etage->setLayout(layout_kl_1_etage);

		layout_klein->addWidget(klein_haus_erste_etage);


		
		QGroupBox* klein_haus_zweite_etage = new QGroupBox(tr("2. Etage"),kleines_haus);

		QHBoxLayout *layout_kl_2_etage = new QHBoxLayout;

		

		QPushButton *btn3 = new QPushButton();
		button_to_led[btn3] = 1;
		button_toggle[btn3] = false;

		layout_kl_2_etage->addWidget(btn3);
		connect(btn3,SIGNAL(pressed()),this,SLOT(btn_pressed()));

		klein_haus_zweite_etage->setLayout(layout_kl_2_etage);

		layout_klein->addWidget(klein_haus_zweite_etage);



	}

	//----------------------------------------Grosses Haus-----------------------------------

	{
		QGroupBox *grosses_haus = new QGroupBox(tr("Grosses Haus"), this);

		mainlayout->addWidget(grosses_haus);

		QVBoxLayout *gr_haus = new QVBoxLayout;

		grosses_haus->setLayout(gr_haus);

		QGroupBox *gr_1 = new QGroupBox(tr("1. Etage"), grosses_haus);
		
		QHBoxLayout *gr_1_layout = new QHBoxLayout();

		gr_1->setLayout(gr_1_layout);


		{
			QPushButton *btn1 = new QPushButton();
			button_to_led[btn1] = 10;
			button_toggle[btn1] = false;

			gr_1_layout->addWidget(btn1);
			connect(btn1,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn2 = new QPushButton();
			button_to_led[btn2] = 8;
			button_toggle[btn2] = false;

			gr_1_layout->addWidget(btn2);
			connect(btn2,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn3 = new QPushButton();
			button_to_led[btn3] = 4;
			button_toggle[btn3] = false;

			gr_1_layout->addWidget(btn3);
			connect(btn3,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn4 = new QPushButton();
			button_to_led[btn4] = 3;
			button_toggle[btn4] = false;

			gr_1_layout->addWidget(btn4);
			connect(btn4,SIGNAL(pressed()),this,SLOT(btn_pressed()));

		}


		QGroupBox *gr_2 = new QGroupBox(tr("2. Etage"), grosses_haus);	

		QHBoxLayout *gr_2_layout = new QHBoxLayout();

		gr_2->setLayout(gr_2_layout);

		{
			QPushButton *btn1 = new QPushButton();
			button_to_led[btn1] = 7;
			button_toggle[btn1] = false;

			gr_2_layout->addWidget(btn1);
			connect(btn1,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn2 = new QPushButton();
			button_to_led[btn2] = 5;
			button_toggle[btn2] = false;

			gr_2_layout->addWidget(btn2);
			connect(btn2,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn3 = new QPushButton();
			button_to_led[btn3] = 6;
			button_toggle[btn3] = false;

			gr_2_layout->addWidget(btn3);
			connect(btn3,SIGNAL(pressed()),this,SLOT(btn_pressed()));


			QPushButton *btn4 = new QPushButton();
			button_to_led[btn4] = 9;
			button_toggle[btn4] = false;

			gr_2_layout->addWidget(btn4);
			connect(btn4,SIGNAL(pressed()),this,SLOT(btn_pressed()));


		}
		gr_haus->addWidget(gr_1);
		gr_haus->addWidget(gr_2);
	}	

	QPushButton *on = new QPushButton(tr("Alles Einschalten"));  
	QPushButton *off = new QPushButton(tr("Alles Aussschalten"));

	mainlayout->addWidget(on);
	mainlayout->addWidget(off);

	connect(on, SIGNAL(pressed()), this, SLOT(everything_on()));
	connect(off, SIGNAL(pressed()), this, SLOT(everything_off()));

	setLayout(mainlayout);
}



void TestLed::btn_pressed()
{
	QPushButton *button = qobject_cast<QPushButton*>(sender());	

	button_toggle[button] = !button_toggle[button];

	API->setLed(button_to_led[button],button_toggle[button]);

	API->flushLed();
}

void TestLed::everything_on()
{
	for(int i = 0; i < 11; i++)
	{
		API->setLed(i,true);
		
	}

	API->flushLed();
	
	for(auto it = button_to_led.begin(); it!= button_to_led.end(); it++)
	{
		(*it) = true;
	}

}

void TestLed::everything_off()
{
	
	for(int i = 0; i < 11; i++)
	{
		API->setLed(i,false);

	}

	API->flushLed();
	
	for(auto it = button_to_led.begin(); it!= button_to_led.end(); it++)
	{
		(*it) = false;
	}

}


