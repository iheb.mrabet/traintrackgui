#include "TestSpeed.h"

#include <QGridLayout>

TestSpeed::TestSpeed(QWidget * parent) : QWidget(parent)
{
    QGridLayout * layout = new QGridLayout;

    label1 = new QLabel(tr("Zug 1"));
    layout->addWidget(label1, 0,0);

    label1_2 = new QLabel(tr("Speed: 0")); 
    layout->addWidget(label1_2,1,0);

    slider1 = new QSlider(Qt::Horizontal);
    slider1->setRange(-100,100);
    layout->addWidget(slider1,2,0);

    button1 = new QPushButton(tr("STOPP"));
    layout->addWidget(button1,3,0);
    
    label2 = new QLabel(tr("Zug 2"));
    layout->addWidget(label2, 4,0);

    label2_2 = new QLabel(tr("Speed: 0")); 
    layout->addWidget(label2_2,5,0);

    slider2 = new QSlider(Qt::Horizontal);
    slider2->setRange(-100,100);
    layout->addWidget(slider2,6,0);

    button2 = new QPushButton(tr("STOPP"));
    layout->addWidget(button2,7,0);
     
    connect(slider1,SIGNAL(sliderReleased()),this,SLOT(slider1changed()));
    connect(slider2,SIGNAL(sliderReleased()),this,SLOT(slider2changed()));
    connect(slider1,SIGNAL(valueChanged(int)),this,SLOT(display1changed(int)));
    connect(slider2,SIGNAL(valueChanged(int)),this,SLOT(display2changed(int)));
    connect(button1,SIGNAL(pressed()), this, SLOT(stop1()));
    connect(button2,SIGNAL(pressed()), this, SLOT(stop2()));
	
    layout->setRowStretch(8,1);

    setLayout(layout);
}


void TestSpeed::slider1changed()
{
    int a = slider1->value();	
    if(a<0)
    {
	API->setDirection(TRAIN_1, BACKWARD);
	API->setSpeed(TRAIN_1,-a);

    }else
    {
	API->setDirection(TRAIN_1, FORWARD);
    	API->setSpeed(TRAIN_1,a);
    }

}

void TestSpeed::slider2changed()
{
    int a = slider2->value();
    if(a<0)
    {
	API->setDirection(TRAIN_2, BACKWARD);
	API->setSpeed(TRAIN_2,-a);

    }else
    {
	API->setDirection(TRAIN_2, FORWARD);
    	API->setSpeed(TRAIN_2,a);
    }

}

void TestSpeed::display1changed(int a)
{
    QString tmp;
    QTextStream (&tmp) << "Speed: " << a <<"%";
    label1_2->setText(tmp);

}

void TestSpeed::display2changed(int a)
{
    QString tmp;
    QTextStream (&tmp) << "Speed: " << a<<"%";
    label2_2->setText(tmp);

}

void TestSpeed::stop1()
{
    API->setSpeed(TRAIN_1,0);
    slider1->setValue(0);
}

void TestSpeed::stop2()
{
    API->setSpeed(TRAIN_2,0);
    slider2->setValue(0);

}

