#ifndef TESTSPEED_H
#define TESTSPEED_H

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QPushButton>

#include "Connector.h"
#include "Logger.h"


class TestSpeed : public QWidget
{
    Q_OBJECT
    public:

        TestSpeed(QWidget * parent);

    public slots:

        void slider1changed();
        void slider2changed();
	void display1changed(int);
	void display2changed(int);
        void stop1();
        void stop2();


    private:

    QSlider *slider1, *slider2;
    QLabel *label1, *label1_2, *label2, *label2_2;
    QPushButton *button1, *button2;

};

#endif //TESTSPEED_H
